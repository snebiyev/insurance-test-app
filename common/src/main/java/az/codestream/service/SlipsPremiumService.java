package az.codestream.service;

import az.codestream.document.SlipsPremium;
import az.codestream.repository.SlipsPremiumRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SlipsPremiumService {
    private final SlipsPremiumRepository slipsPremiumRepository;
    public List<SlipsPremium> findAll() {
        return slipsPremiumRepository.findAll();
    }
}
