package az.codestream.service;

import az.codestream.document.Country;
import az.codestream.repository.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountryService {
    private final CountryRepository repository;

    public Country findById(String id) {
        return repository.findById(id).orElse(null);
    }
}
