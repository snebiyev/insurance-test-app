package az.codestream.service;

import az.codestream.repository.CaseNotLifePremiumRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CaseNotLifePremiumService {
    private final CaseNotLifePremiumRepository repository;
}
