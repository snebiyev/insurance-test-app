package az.codestream.service;

import az.codestream.document.Branch;
import az.codestream.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchService {
    private final BranchRepository branchRepository;

    public Branch getBranch(String id) {
        return branchRepository.findById(id).orElse(null);
    }
    public List<Branch> getBranchByParentId(String id) {
        return branchRepository.findBranchesByParentBranchIdIs(new ObjectId(id));
    }
}
