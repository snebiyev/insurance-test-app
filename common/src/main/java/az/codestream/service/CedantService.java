package az.codestream.service;

import az.codestream.document.Cedant;
import az.codestream.repository.CedantRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CedantService {
    private final CedantRepository repository;

    public List<Cedant> findByCountryIdAndTypesCedantsId(List<String> countriesId, List<String> typesCedantsId) {
        List<ObjectId> countriesIdObjectId = countriesId.stream().map(ObjectId::new).collect(Collectors.toList());
        List<ObjectId> typesCedantsIdObjectId = typesCedantsId.stream().map(ObjectId::new).collect(Collectors.toList());
        return repository.findByCountriesIdInAndGroupsCedantsIdIn(countriesIdObjectId, typesCedantsIdObjectId);
    }

    public List<Cedant> findByCountryId(List<String> countriesId) {
        List<ObjectId> countriesIdObjectId = countriesId.stream().map(ObjectId::new).collect(Collectors.toList());
        return repository.findByCountriesIdIn(countriesIdObjectId);
    }
}
