package az.codestream.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SlipsFilterRequestDto {

    List<String> countries;
    List<String> groups;
    List<String> branchTypes;
    @NotEmpty(message = "Cedants are required")
    List<String> cedants;
    List<String> validationStatuses;
    List<String> confirmationStatuses;
    String publishedDate;
    @NotEmpty(message = "MainBranch is required")
    String mainBranch;
}
