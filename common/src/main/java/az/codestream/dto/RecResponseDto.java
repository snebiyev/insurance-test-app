package az.codestream.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RecResponseDto {
    private String reference;
    private String country;
    private String cedant;
    private String validationStatus;
    private String confirmationStatus;
    private String publishedDate;
    private String branch;
    private Double rec;
}
