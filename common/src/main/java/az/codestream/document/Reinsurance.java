package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reinsurance")
@RequiredArgsConstructor
@Getter
@Setter
public class Reinsurance {
    @Id
    private String id;
}
