package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document(collection = "branches")
@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class Branch {
    @Id
    private String id;
    private final String[] alias;
    private final String code;
    @Field("created_at")
    private final LocalDateTime createdAt;
    @Field("is_parent")
    private final Integer isParent;
    private final String name;
    @Field("parent_branch_id")
    private final String parentBranchId;
    private final Integer status;
    private final String type;
    @Field("updated_at")
    private final LocalDateTime updatedAt;
}
