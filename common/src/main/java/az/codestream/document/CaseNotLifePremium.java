package az.codestream.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZonedDateTime;

@Document(collection = "case_not_life_premium")
@Data
public class CaseNotLifePremium {
    @Id
    private String id;
    @Field("active_status")
    private Integer activeStatus;
    @Field("branch")
    private String branch;
    @Field("branches_id")
    private String branchesId;
    @Field("case_validation_status")
    private String caseValidationStatus;
    @Field("case_variant")
    private String caseVariant;
    @Field("category")
    private String category;
    @Field("cedants_id")
    private String cedantsId;
    @Field("commission_cession")
    private Double commissionCession;
    @Field("commission_refunded")
    private Integer commissionRefunded;
    @Field("date_effective")
    private String dateEffective;
    @Field("date_transaction")
    private String dateTransaction;
    @Field("deadline")
    private String deadline;
    @Field("insured_capital")
    private Double insuredCapital;
    @Field("invoiced_premium")
    private Double invoicedPremium;
    @Field("nature_risque_id")
    private String natureRisqueId;
    @Field("net_amount")
    private Double netAmount;
    @Field("paid_commission")
    private Double paidCommission;
    @Field("part_cedant_coass")
    private Integer partCedantCoass;
    @Field("policy_number")
    private String policyNumber;
    @Field("premium_ceded")
    private Double premiumCeded;
    @Field("premium_ht")
    private Integer premiumHt;
    @Field("prime_net_ceded")
    private Double primeNetCeded;
    @Field("slipes_prime_id")
    private String slipesPrimeId;
    @Field("sub_branches_id")
    private String subBranchesId;
    @Field("updated_at")
    private ZonedDateTime updatedAt;
}
