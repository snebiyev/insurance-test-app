package az.codestream.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
@Document(collection = "slips_premium")
@Data
public class SlipsPremium {
    @Id
    private String id;
    @Field("approval_status")
    private String approvalStatus;
    @Field("cedants_id")
    private String cedantsId;
    @Field("cedants_type_id")
    private String cedantsTypeId;
    @Field("commission_refunded_total")
    private String commissionRefundedTotal;
    @Field("confirmation_date")
    private String confirmationDate;
    @Field("confirmation_status")
    private String confirmationStatus;
    @Field("created_at")
    private LocalDateTime createdAt;
    @Field("edited_period")
    private String editedPeriod;
    @Field("file_url")
    private String fileUrl;
    @Field("invoice_premium_total")
    private Double invoicePremiumTotal;
    @Field("is_paid")
    private Integer isPaid;
    @Field("net_amount_total")
    private Double netAmountTotal;
    @Field("published_date")
    private String publishedDate;
    @Field("reference")
    private String reference;
    @Field("reinsurances_id")
    private String reinsurancesId;
    @Field("slip_type")
    private String slipType;
    @Field("update_progress")
    private Integer updateProgress;
    @Field("update_status")
    private Integer updateStatus;
    @Field("updated_at")
    private LocalDateTime updatedAt;
    @Field("user_cedant_id")
    private String userCedantId;
    @Field("validation_date")
    private String validationDate;
    @Field("validation_status")
    private String validationStatus;
    @Field("warnings_saved")
    private Integer warningsSaved;
}
