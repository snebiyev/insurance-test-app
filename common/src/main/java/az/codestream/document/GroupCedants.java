package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZonedDateTime;

@Document(collection = "groups_cedants")
@RequiredArgsConstructor
@Getter
@Setter
public class GroupCedants {
    @Id
    private String id;
    private String name;
    @Field("created_at")
    private ZonedDateTime createdAt;
    @Field("updated_at")
    private ZonedDateTime updatedAt;
}
