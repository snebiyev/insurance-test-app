package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZonedDateTime;

@Document(collection = "cedants")
@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class Cedant {
    @Id
    private String id;
    private final String code;
    private final String name;
    private final String logo;
    private final String contact;
    private final String email;
    @Field("region_id")
    private final String regionId;
    @Field("countries_id")
    @DocumentReference(lazy = true)
    private final Country countriesId;
    @Field("reinsurances_id")
    private final String reinsurancesId;
    @Field("groups_cedants_id")
    private final String groupsCedantsId;
    @Field("currencies_id")
    private final String currenciesId;
    @Field("types_cedants_id")
    private final String typesCedantsId;
    @Field("created_at")
    private final ZonedDateTime createdAt;
    @Field("updated_at")
    private final ZonedDateTime updatedAt;
    @Field("benefit_percentage")
    private final String benefitPercentage;
    @Field("estimation_type")
    private final String estimationType;
    private final String color1;
    private final String color2;


}
