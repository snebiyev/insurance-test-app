package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZonedDateTime;

@Document(collection = "countries")
@RequiredArgsConstructor
@Getter
@Setter
public class Country {
    @Id
    private String id;
    private String code;
    private String name;
    @Field("regions_id")
    private String regionsId;
    @Field("updated_at")
    private ZonedDateTime updatedAt;

}
