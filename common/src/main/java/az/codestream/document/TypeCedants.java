package az.codestream.document;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "typecedants")
@RequiredArgsConstructor
@Getter
@Setter
public class TypeCedants {
    @Id
    private String id;
}
