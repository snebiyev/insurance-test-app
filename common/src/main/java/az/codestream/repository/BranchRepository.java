package az.codestream.repository;

import az.codestream.document.Branch;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BranchRepository extends MongoRepository<Branch, String> {
    List<Branch> findBranchesByParentBranchIdIs(ObjectId id);
}
