package az.codestream.repository;

import az.codestream.document.SlipsPremium;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SlipsPremiumRepository extends MongoRepository<SlipsPremium,String> {

}
