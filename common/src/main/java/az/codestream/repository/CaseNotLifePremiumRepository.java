package az.codestream.repository;

import az.codestream.document.CaseNotLifePremium;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CaseNotLifePremiumRepository extends MongoRepository<CaseNotLifePremium, String> {

}
