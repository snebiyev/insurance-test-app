package az.codestream.repository;

import az.codestream.document.Cedant;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CedantRepository extends MongoRepository<Cedant, String> {
    List<Cedant> findByCountriesIdIn(List<ObjectId> countriesId);
    List<Cedant> findByCountriesIdInAndGroupsCedantsIdIn(List<ObjectId> countriesId, List<ObjectId> groupsCedantsId);
}
