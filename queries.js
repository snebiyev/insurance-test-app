// Get cedants by country (COTE D’IVOIRE)
/*
    db.cedants.find({
        countries_id:ObjectId("5da8198f329d87383208ea42")
        })
 */

// Get companies by type ('not life')
/*
    db.branches.find({type:{$in:['not life']}})
 */

// Get companies ( Cedants)
/*
db.cedants.find({_id:{$in:[ObjectId("5de7d0ed329d8746112bef92")
                            ,ObjectId("5ebbfec8329d8763057a4014")
                            ,ObjectId("5ec3e07e329d8748f31aa8c2")]}})

 */


// Reference - SlipsPremium.reference
// Country - Country.id
// Cedants - Cedants.id
// Validation status - SlipsPremium.validationStatus
// Confirmation status - SlipsPremium.confirmationStatus
// Publication date - SlipsPremium.publishedDate
// Branche - branches - sub of selected main branches

// Calculated REC -
//    db.case_not_life_premium.aggregate([
//    {$group: {_id:{cedants_id:"$cedants_id",slips_id:"$slipes_prime_id",branch_id:"$branches_id",rec:{$multiply: [ 0.36, {$sum: "$premium_ht"} ]}}}}
//    ])
// slips_id - SlipsPremium.id
// branch_id - branches - sub of selected main branches
// $cedants_id -

// Response
/*
db.case_not_life_premium.aggregate([
{
$match:
    {
        'slipes_prime_id': { $in: [ObjectId("5f9d7f1a329d871460566a22"), ObjectId("5f9d78cd329d8713893ff164")] }
        , 'branches_id': { $in: [ObjectId("5da6d682329d8720647c9cc2")] }
       , 'cedants_id': { $in: [ObjectId("5ebbfec8329d8763057a4014"),ObjectId("5de7d0ed329d8746112bef92"),ObjectId("5ec3e07e329d8748f31aa8c2")] }
    }
},
{ $group: { _id: { cedants_id: "$cedants_id", slips_id: "$slipes_prime_id", branch_id: "$branches_id", rec: { $multiply: [0.36, { $sum: "$premium_ht" }] } } } }
])
 */

/*
db.case_not_life_premium.aggregate([
    //    { $project: { cedants_id: 1, slipes_prime_id: 1, branches_id: 1,premium_ht:1 } },
    {
        $match:
            {
                'slipes_prime_id': { $in: [ObjectId("5f9d7f1a329d871460566a22"), ObjectId("5f9d78cd329d8713893ff164")] }
                , 'branches_id': { $in: [ObjectId("5da6d682329d8720647c9cc2")] }
                , 'cedants_id': { $in: [ObjectId("5ebbfec8329d8763057a4014"), ObjectId("5de7d0ed329d8746112bef92"), ObjectId("5ec3e07e329d8748f31aa8c2")] }

            },

    },

    { $group: { _id: { cedants_id: "$cedants_id", slips_id: "$slipes_prime_id", branch_id: "$branches_id" }, rec: { $sum: { $multiply: [0.36, "$premium_ht"] } } } }
//    ,{
//        $lookup:
//            {
//                from: "branches",
//                localField: "_id.branch_id",
//                foreignField: "_id",
//                as: "branch_name"
//            }
//    }

])


 */
