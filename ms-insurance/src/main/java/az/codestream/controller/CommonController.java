package az.codestream.controller;

import az.codestream.document.SlipsPremium;
import az.codestream.dto.SlipsFilterRequestDto;
import az.codestream.service.CommonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CommonController {
    CommonService service;

    @PostMapping("/calculate")
    @Operation(summary = "Find calculated rec by filter",
            description = "Find calculated record by given filter result (based on branch_id, slips_id, cedant_id)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "No data found based on given filter")})
    public ResponseEntity<?> findAllSlips(@RequestBody @Validated SlipsFilterRequestDto requestDto) {

        List<SlipsPremium> premiumList = service.findSlips(requestDto);
        if (premiumList == null || premiumList.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found");
        }

        List<ObjectId> slipsIds = premiumList.stream().map(slipsPremium -> new ObjectId(slipsPremium.getId())).collect(Collectors.toList());
        List<ObjectId> cedantsIds = premiumList.stream().map(slipsPremium -> new ObjectId(slipsPremium.getCedantsId())).collect(Collectors.toList());
        if (slipsIds.isEmpty() || cedantsIds.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found");
        }

        List<Document> documents = service.calculateByFilter(slipsIds, cedantsIds, requestDto.getMainBranch(), "case_not_life_premium");
        if (documents == null || documents.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found");
        }
        return ResponseEntity.ok(documents);

    }
}
