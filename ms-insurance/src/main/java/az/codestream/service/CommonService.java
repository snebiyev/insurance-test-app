package az.codestream.service;

import az.codestream.document.SlipsPremium;
import az.codestream.dto.SlipsFilterRequestDto;
import com.mongodb.client.MongoCollection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonService {
    private final MongoTemplate mongoTemplate;

    public List<SlipsPremium> findSlips(SlipsFilterRequestDto slipsFilterRequestDto) {
        Query query = new Query();
        //        query.fields().include("reference", "cedants_id", "country", "validation_status", "confirmation_status", "published_date");
        if (slipsFilterRequestDto.getValidationStatuses() != null) {
            query.addCriteria(Criteria.where("validation_status").in(slipsFilterRequestDto.getValidationStatuses()));
        }
        if (slipsFilterRequestDto.getConfirmationStatuses() != null) {
            query.addCriteria(Criteria.where("confirmation_status").in(slipsFilterRequestDto.getConfirmationStatuses()));
        }
        if (slipsFilterRequestDto.getPublishedDate() != null) {
            //{published_date: {$gte: "2020-01-01", $lte: "2020-01-31"}}
            query.addCriteria(Criteria.where("published_date").gte(slipsFilterRequestDto.getPublishedDate())
                    .lt(LocalDate.parse(slipsFilterRequestDto.getPublishedDate()).plusDays(1).toString()));
        }
        if (slipsFilterRequestDto.getCedants() != null) {
            query.addCriteria(Criteria.where("cedants_id").in(slipsFilterRequestDto.getCedants().stream().map(ObjectId::new).toArray()));
        }
        return mongoTemplate.find(query, SlipsPremium.class);
    }

    public List<Document> calculateByFilter(List<ObjectId> slipsIds, List<ObjectId> cedantsIds, String branchId, String collectionName) {
        MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
        List<? extends Bson> pipeline = Arrays.asList(
                new Document()
                        .append("$project", new Document()
                                .append("cedants_id", 1.0)
                                .append("slipes_prime_id", 1.0)
                                .append("branches_id", 1.0)
                                .append("premium_ht", 1.0)
                        ),
                new Document()
                        .append("$match", new Document()
                                .append("slipes_prime_id", new Document()
                                        .append("$in", slipsIds)
                                )
                                .append("branches_id", new Document()
                                        .append("$in", List.of(new ObjectId(branchId)))
                                )
                                .append("cedants_id", new Document()
                                        .append("$in", cedantsIds)
                                )
                        ),
                new Document()
                        .append("$group", new Document()
                                .append("_id", new Document()
                                        .append("cedants_id", "$cedants_id")
                                        .append("slips_id", "$slipes_prime_id")
                                        .append("branch_id", "$branches_id")
                                )
                                .append("rec", new Document()
                                        .append("$sum", new Document()
                                                .append("$multiply", Arrays.asList(
                                                                0.36,
                                                                "$premium_ht"
                                                        )
                                                )
                                        )
                                )
                        )
        );
        ArrayList<Document> documents = collection.aggregate(pipeline).into(new ArrayList<>());
        documents.forEach(document -> log.info("{}", document));
        return documents;
    }
}
