**Insurance calculator API**

**About**

This is an API for calculating insurance risk costs.

**How to run**

* First run Docker container using `docker-compose.yml` file.
    * To run Service, MongoDb and Mongo Express (GUI for MongoDB) as Docker container, you need to have Docker installed.
    * Run `./gradlew make` to build the project.
    * Go to project root directory and run `docker-compose up -d`
    * Open Mongo Express (http://localhost:9001/), create new DB with name `insurance`
    * Create new collections based on test-collection json file names (root folder->test-collection folder)
    * Import every json for relevant collection (We may automate last 3 steps in future)
    * Now service runs on http://localhost:8080/

**How to use**

* You can test the API with Postman.
    * You can find the Postman collection in `postman` folder.
        * There are two files (import them into Postman):
            * `Postman Environment Variables`
            * `Postman Collection file`
    * Or you can test it via the Swagger UI.
        * Run service
        * Open the browser and go to `http://localhost:8080/swagger.html`
        * Examples of request body
          ```json
                {
                 "countries": [
                    "5da8198f329d87383208ea42"
                 ],
                 "groups": [
                    "5d8213146f611f1df61cda23"
                  ],
                 "mainBranch": "5da6d682329d8720647c9cc2",
                 "cedants": [
                   "5de7d0ed329d8746112bef92",
                   "5ebbfec8329d8763057a4014",
                   "5ec3e07e329d8748f31aa8c2"
                  ],
                 "validationStatuses": [
                   "Pending",
                   "Partial Verified",
                   "Verified",
                   "Rejected"
                  ],
                 "confirmationStatuses": [
                   "Confirmed",
                   "Rejected",
                   "Pending"
                  ]
                 }
            ```
            
